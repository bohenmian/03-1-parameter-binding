package com.twuc.webApp.model;


import javax.validation.constraints.*;

public class Person {

    @Size(min = 3, max = 10)
    private String name;

    @NotNull
    private String gender;

    @Email
    private String email;

    @Min(1000)
    @Max(9999)
    private Integer yearOfBirth;

    public Person() {
    }

    public Person(@Size(min = 3, max = 10) String name, @NotNull String gender, @Email String email, @Min(1000) @Max(9999) Integer yearOfBirth) {
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.yearOfBirth = yearOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }
}
