package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.model.Contract;
import com.twuc.webApp.model.LocalDataTime;
import com.twuc.webApp.model.Person;
import com.twuc.webApp.model.Student;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;


@RestController
@RequestMapping("/api")
public class UserController {

    private ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping("/users/{userId}")
    public String getBook(@PathVariable("userId") Integer userId) {
        return "this user id is " + userId;
    }

    @GetMapping("/user/{id}")
    public String getUserInformation(@PathVariable("id") int id) {
        return "this user id is " + id;
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public String getUserAndBookInformation(@PathVariable("bookId") Integer bookId, @PathVariable("userId") Integer userId) {
        return "this user id is " + userId + "," + " this book id is " + bookId;
    }

    @GetMapping("/user")
    public String getStudent(@RequestParam(value = "name", defaultValue = "jack") String name,
                             @RequestParam(value = "class", defaultValue = "5") String klass) {
        return "my name is " + name + ",I am from class " + klass;
    }

    @GetMapping("/user/collection")
    public String getStudentIds(@RequestParam List<String> ids) {
        return "students id " + ids;
    }

    @PostMapping("/contract/serialize")
    public String serializeContract(@RequestBody Contract contract) throws JsonProcessingException {
        return objectMapper.writeValueAsString(contract);
    }

    @GetMapping("/contract/deserialize")
    public Contract deserializeContract(@RequestParam String json) throws IOException {
        return objectMapper.readValue(json, Contract.class);
    }

    @PostMapping("/student/serialize")
    public String serializeStudent(@RequestBody Student student) throws JsonProcessingException {
        return objectMapper.writeValueAsString(student);
    }

    @PostMapping("/datetimes")
    public LocalDataTime deserializeTime(@RequestBody LocalDataTime dataTime) {
        return dataTime;
    }

    @PostMapping("/person")
    public String createPerson(@RequestBody @Valid Person person) {
        return person.getName();
    }

}

