package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.model.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void should_return_user_information_when_parameter_type_is_integer() throws Exception {
        mockMvc.perform(get("/api/users/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("this user id is 1"));

    }

    @Test
    void should_return_user_information_when_parameter_type_is_int() throws Exception {
        mockMvc.perform(get("/api/user/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("this user id is 1"));
    }

    @Test
    void should_return_user_information_and_book_information_when_two_parameter() throws Exception {
        mockMvc.perform(get("/api/users/1/books/2"))
                .andExpect(status().isOk())
                .andExpect(content().string("this user id is 1, this book id is 2"));
    }

    @Test
    void should_return_user_information_when_give_username_and_class() throws Exception {
        mockMvc.perform(get("/api/user")
                .param("name", "tom")
                .param("class", "4"))
                .andExpect(status().isOk())
                .andExpect(content().string("my name is tom,I am from class 4"));
    }

    @Test
    void should_return_default_user_information_when_give_username_and_class() throws Exception {
        mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andExpect(content().string("my name is jack,I am from class 5"));
    }

    @Test
    void should_return_all_user_information_when_give_collection() throws Exception {
        mockMvc.perform(get("/api/user/collection").param("ids", "1,2,3"))
                .andExpect(status().isOk())
                .andExpect(content().string("students id [1, 2, 3]"));
    }

    @Test
    void should_return_json_when_serialize() throws Exception {
        mockMvc.perform(post("/api/contract/serialize")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{\"name\":\"tom\",\"year\":2019}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("tom"));
    }

    @Test
    void should_return_object_when_deserialize() throws Exception {
        mockMvc.perform(get("/api/contract/deserialize")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param("json", "{\"name\":\"tom\",\"year\":2019}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("tom"))
                .andExpect(jsonPath("$.year").value(2019));
    }

    @Test
    void should_return_object_when_no_setter() throws Exception {
        mockMvc.perform(post("/api/student/serialize")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{\"name\":\"tom\",\"gender\":\"male\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("tom"));
    }

    @Test
    void should_return_local_datatime() throws Exception {
        mockMvc.perform(post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_the_gender_when_input_is_not_null() throws Exception {
        Person person = new Person("jack", "male", "bohenmian@gmail.com", 1111);
        mockMvc.perform(post("/api/person")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(person)))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_the_gender_when_input_is_null() throws Exception {
        Person person = new Person("jack", null, "bohenmian@gmail.com", 1111);
        mockMvc.perform(post("/api/person")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(person)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_the_person_name_when_name_size_more_than_3_and_less_than_10() throws Exception {
        Person person = new Person("jack", "male", "bohenmian@gmail.com", 1111);
        mockMvc.perform(post("/api/person")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(person)))
                .andExpect(status().isOk())
                .andExpect(content().string("jack"));
    }

    @Test
    void should_throw_error_code_when_yearOfBirth_less_than_1000() throws Exception {
        Person person = new Person("jack", "male", "bohenmian@gmail.com", 999);
        mockMvc.perform(post("/api/person")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(person)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_throw_error_code_when_input_email_is_invalid() throws Exception {
        Person person = new Person("jack", "male", "bgmail.com", 1999);
        mockMvc.perform(post("/api/person")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsBytes(person)))
                .andExpect(status().isBadRequest());
    }




}
